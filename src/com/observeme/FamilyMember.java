package com.observeme;

public class FamilyMember implements Observer {
    public void update() {
        System.out.println("Family member is notified");
    }
}