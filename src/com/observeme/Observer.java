package com.observeme;

public interface Observer {
    void update();
}
