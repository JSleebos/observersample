package com.observeme;

public class Main {

    public static void main(String[] args) {
        Subject netflix = new Netflix();

        Observer anne = new NormalMember();
        Observer jurjen = new FamilyMember();

        System.out.println("\nNormal and family member added.");
        netflix.subscribe(anne);
        netflix.subscribe(jurjen);

        netflix.notifySubscribers();

        System.out.println("\nFamily member removed");
        netflix.unSubscribe(jurjen);

        netflix.notifySubscribers();

        System.out.println("\nFamily member added again");
        netflix.subscribe(jurjen);

        netflix.notifySubscribers();
    }
}
