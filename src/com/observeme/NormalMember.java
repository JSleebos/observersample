package com.observeme;

public class NormalMember implements Observer {

    public void update() {
        System.out.println("Normal member is notified");
    }
}
