package com.observeme;

import java.util.ArrayList;
import java.util.List;

public class Netflix implements Subject {
    List<Observer> users = new ArrayList<>();

    public void subscribe(Observer user) {
        users.add(user);
    }

    public void unSubscribe(Observer user) {
        users.remove(user);
    }

    public void notifySubscribers() {
        System.out.println("New content has been added tot Netflix");
        for (Observer user : users) {
            user.update();
        }
    }
}
