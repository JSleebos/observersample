package com.observeme;

public interface Subject {
    void subscribe(Observer user);
    void unSubscribe(Observer user);
    void notifySubscribers();
}
